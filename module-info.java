module GalleryManagment {
    requires javafx.controls;
    requires javafx.base;
    requires javafx.graphics;
    requires javafx.fxml;

    opens manager.artistManagement;
}