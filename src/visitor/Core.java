package visitor;

import ntt.ArtWork;

import java.util.List;

/**
 * this class returns next and previous image
 * and can be sets and initialize with a list that gives from Filter class
 * @author iman
 */
public class Core {
    private static Core core = new Core();
    private List<ArtWork> artWorks;
    private int index = -1;

    public static Core get(){
        return core;
    }

    public void setList(List<ArtWork> artWorks){
        index = -1;
        this.artWorks = artWorks;
    }

    public ArtWork next(){
        nextIndex();
        return artWorks.get(index);

    }

    public ArtWork previous(){
        previousIndex();
        return artWorks.get(index);
    }

    private void previousIndex() {
        index--;
        if (index == -1)
            index = artWorks.size() - 1;
    }

    private void nextIndex() {
        index++;
        if (index == artWorks.size())
            index = 0;
    }
}
