package visitor;

import ntt.ArtWork;
import ntt.Artist;
import ntt.Genre;
import ntt.Type;

import java.util.ArrayList;
import java.util.List;

/**
 * this class get status from GUI comboBox and and returns a list to core
 * @author iman
 */
public class Filter {

    private List<ArtWork> mainArtWorks;
    private List<ArtWork> displayArtworks;
    private static Filter filter = new Filter();

    public void init(List<ArtWork> artWorks) {
        this.mainArtWorks = artWorks;
    }

    public static Filter get(){
        return filter;
    }

    public List<ArtWork> setFilter (Artist artist, Genre genre, Type type){
        displayArtworks = new ArrayList<>();
        displayArtworks.addAll(mainArtWorks);
        if (artist != null){
            setFilter(artist);
        }if (genre != null){
            setFilter(genre);
        }if (type != null){
            setFilter(type);
        }
        return displayArtworks;
    }

    private void setFilter(Type type) {
        for (int i = 0; i < displayArtworks.size(); i++){
            if (!displayArtworks.get(i).getType_artwork().equals(type)){
                displayArtworks.remove(i);
                i--;
            }
        }
    }

    private void setFilter(Genre genre) {
        for (int i = 0; i < displayArtworks.size(); i++){
            if (!displayArtworks.get(i).getType_artwork().equals(genre)){
                displayArtworks.remove(i);
                i--;
            }
        }
    }

    private void setFilter(Artist artist) {
        for (int i = 0; i < displayArtworks.size(); i++){
            if (!displayArtworks.get(i).getType_artwork().equals(artist)){
                displayArtworks.remove(i);
                i--;
            }
        }
    }
}
