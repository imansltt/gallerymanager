package visitor;

import DAO.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import ntt.*;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


/**
 * this class controls fxml view
 * @author iman
 */
public class VisitorController implements Initializable {
    @FXML private AnchorPane root;
    @FXML ImageView imageView;
    @FXML Button nextbtn, prevbtn;
    @FXML TextArea descriptionArea;
    @FXML Label artistName, artistAge, price;
    @FXML ComboBox<String> artistComboBox, genreComboBox, typeComboBox;


    private DataBase db = DataBase.getConnection("root", "root", "agm");
    private Connection connection = db.connect();
    private ArtistDAO artistDAO = new ArtistDAO(connection);
    private GenreDAO genreDAO = new GenreDAO(connection);
    private TypeDAO typeDAO = new TypeDAO(connection);
    private List<Artist> artists;
    private List<Genre> genres;
    private List<Type> types;

    private Event currentEvent;
    private Filter filter = new Filter();
    private Core core = new Core();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /// show a popup for user to enter event id and load it

        //assign current Event to data base with EventDAO
//        artists = artistDAO.findAll();
//        genres = genreDAO.findAllGenres();
//        types = typeDAO.findAllTypes();

        testInit();

        filter.init(currentEvent.getArtWorks());
        core.setList(filter.setFilter(null, null, null));
        try {
            loadArtwork(core.next());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void testInit() {
        artists = new ArrayList<>();
        ////////////////////////////////////
        artists.add(new Artist(1, "iman" , "sultani" , "image Source" , 19));
        artists.add(new Artist(2, "amirAli", "khaneAnGha" , "imagesource", 19));
        types = new ArrayList<>();
        ////////////////////////////////////
        types.add(new Type(1, "natural"));
        types.add(new Type(2, "city"));
        genres = new ArrayList<>();
        ////////////////////////////////////
        genres.add(new Genre(1, "close up"));
        genres.add(new Genre(2, "landscape"));
        ArrayList<ArtWork> artWorksTest= new ArrayList<>();
        //////////////////////////////////
        artWorksTest.add(new ArtWork(1, artists.get(0), "Bee", "a closeUp of a Bee", 0.0, types.get(0), genres.get(0), "./images/Bee.JPG"));
        artWorksTest.add(new ArtWork(2, artists.get(0), "Aghaghia", "Aghaghia Tree in a shot", 0.0, types.get(1), genres.get(0), "./images/Aghaghia.JPG"));
        artWorksTest.add(new ArtWork(3, artists.get(0), "BeeOnYellow", "a Bee on a Little Yellow Flower", 0.0, types.get(0), genres.get(0), "./images/BeeOnYellow.JPG"));
        artWorksTest.add(new ArtWork(4, artists.get(1), "Marijuana", "CloseUp of a Marijuana.", 0.0, types.get(0), genres.get(0), "./images/Marijuana.JPG"));
        artWorksTest.add(new ArtWork(5, artists.get(0), "marjanAbadSunSet", "sun set in MarjanAbad in a garden.", 0.0, types.get(1), genres.get(0), "./images/MarjanAbadSunSet.JPG"));
        artWorksTest.add(new ArtWork(6, artists.get(0), "MiladTower", "Milad Tower in view of south tehran", 0.0, types.get(1), genres.get(1), "./images/MiladTower.JPG"));
        artWorksTest.add(new ArtWork(7, artists.get(0), "SpiderRasht", "Spider!", 0.0, types.get(1), genres.get(0), "./images/SpiderRasht.JPG"));
        //////////////////////////////////////
        currentEvent = new Event(artWorksTest);
    }

    public void prevButton() throws SQLException {
        loadArtwork(core.previous());
    }

    public void nextButton() throws SQLException {
        loadArtwork(core.next());
    }

    private void loadArtwork(ArtWork artWork) throws SQLException {
        ArtistDAO artistDAO = new ArtistDAO(connection);
        Artist a = artistDAO.find(artWork.getArtist().getId());

        artistAge.setText(a.getAge().toString());
        artistName.setText(a.getFname() + " " + a.getLname());

        price.setText(artWork.getPrice().toString());

        descriptionArea.setText(artWork.getDescription());

        File file = new File(artWork.getAddress());
        Image image = new Image(file.toURI().toString());
        imageView.setImage(image);
    }


    public void submit(ActionEvent actionEvent) throws SQLException {
        Artist artist = findArtist(artistComboBox.getValue());
        Genre genre = findGenre(artistComboBox.getValue());
        Type type = findType(artistComboBox.getValue());
        core.setList(filter.setFilter(artist, genre, type));
        loadArtwork(core.next());
    }

    private Type findType(String name) {
        for (int i = 0 ; i < types.size(); i++){
            if (name.equals(types.get(i).toString()))
                return types.get(i);
        }
        return null;
    }

    private Genre findGenre(String name) {
        for (int i = 0 ; i < genres.size(); i++){
            if (name.equals(genres.get(i).toString()))
                return genres.get(i);
        }
        return null;
    }

    private Artist findArtist(String name) {
        for (int i = 0 ; i < artists.size(); i++){
            if (name.equals(artists.get(i).toString()))
                return artists.get(i);
        }
        return null;
    }
}
