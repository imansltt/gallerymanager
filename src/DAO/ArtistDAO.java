package DAO;

import ntt.Artist;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ArtistDAO {


//    private static ArtistDAO artistDAO = null;
//
//    private ArtistDAO() {
//        if (artistDAO == null)
//            this.artistDAO = new ArtistDAO();
//    }
//
//    public static ArtistDAO getArtistDAO(){
//        return artistDAO;
//    }

    enum Statement {
        FIND,
        FIND_ALL,
        ADD,
        REMOVE,
        EDIT
    }

    private Map<Statement, PreparedStatement> statements =  new HashMap<>();
    private Connection connection;

    public ArtistDAO(Connection connection){
        if (connection == null) {
            throw new IllegalArgumentException();
        }
        this.connection = connection;

        prepareStatements();
    }

    private void prepareStatements() {
        try {
            statements.put(Statement.FIND_ALL, this.connection.prepareStatement(
                    "select * from artist"));
            statements.put(Statement.FIND, this.connection.prepareStatement(
                    "select * from artist where id = ?"));
            statements.put(Statement.ADD, this.connection.prepareStatement(
                    "insert into artist values (null, ?, ?, ?, ?)"));
            statements.put(Statement.EDIT, this.connection.prepareStatement(
                    "update artist set fname = ?, lname = ?, image_src = ?, age = ?, where id = ?"));
            statements.put(Statement.REMOVE, this.connection.prepareStatement(
                    "delete from artist where id = ?"));
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void add(Artist artist) {
        PreparedStatement addArtist = statements.get(Statement.ADD);
        try {
            addArtist.setString(1, artist.getFname());
            addArtist.setString(2, artist.getLname());
            addArtist.setString(3, artist.getImage_src());
            addArtist.setInt(4, artist.getAge());
            addArtist.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void remove(Integer id) {
        try {
            statements.get(Statement.REMOVE).setInt(1, id);
            statements.get(Statement.REMOVE).executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void edit(Artist artist) {
        PreparedStatement editStatement = statements.get(Statement.EDIT);
        try {
            editStatement.setString(1, artist.getFname());
            editStatement.setString(2, artist.getLname());
            editStatement.setString(3, artist.getImage_src());
            editStatement.setInt(4, artist.getAge());
            editStatement.setInt(5, artist.getId());
            editStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ArrayList<Artist> findAll() {
        ArrayList<Artist> artists = new ArrayList<>();
        try (ResultSet resultSet = statements.get(Statement.FIND_ALL).executeQuery()){
            while (resultSet.next()) {
                Artist artist = new Artist(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5));
                artists.add(artist);
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return artists;
    }

    public Artist find(int id) throws SQLException {

        statements.get(Statement.FIND).setInt(1, id);
        ResultSet resultSet = statements.get(Statement.FIND).executeQuery();
        if (resultSet.next()){
            return new Artist(
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getInt(5));
        }else
            throw new SQLException();
    }

}
