package manager.artistManagement;

import DAO.ArtistDAO;
import ntt.Artist;

import java.sql.*;

public class ArtistMenu {
    public static final String DB_URL = "jdbc:mysql://localhost:3306/gallery";
    public static final String USER_NAME = "root";
    public static final String PASSWORD = "PASSWORD";
    private ArtistForm artistForm;
    private ArtistDAO artistDAO;
    private Connection connection;

    public ArtistMenu(Connection connection, ArtistForm artistForm) {
        try {
            connection = DriverManager.getConnection(
                    DB_URL, USER_NAME, PASSWORD);

            artistDAO = new ArtistDAO(connection);


        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    private void initForm () {
        artistForm = new ArtistForm();
    }

    private void add(Artist artist){
        artistDAO.add(artist);
    }

    private void remove (int id){
        artistDAO.remove(id);
    }

    private void edit(Artist artist){
        //loading to edit form
        artistDAO.edit(artist);
    }

    private void update(){
        //update table list
    }
}
