package manager.artistManagement;

import javafx.scene.control.*;
import javafx.scene.image.ImageView;

public class ArtistForm {
    private Button addBtn;
    private Button removeBtn;
    private Button editBtn;
    private TableView table;
    private TextField name;
    private TextField age;
    private ImageView image;
    private Button addPicture;

    public ArtistForm (){

    }

    public Button getAddBtn() {
        return addBtn;
    }

    public Button getRemoveBtn() {
        return removeBtn;
    }

    public Button getEditBtn() {
        return editBtn;
    }

    public TableView getTable() {
        return table;
    }

    public TextField getName() {
        return name;
    }

    public TextField getAge() {
        return age;
    }

    public ImageView getImage() {
        return image;
    }

    public Button getAddPicture() {
        return addPicture;
    }
}
