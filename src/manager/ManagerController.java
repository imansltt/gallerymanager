package manager;

import DAO.ArtistDAO;
import DAO.DataBase;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
///////////////////////////////////////
import ntt.*;
import DAO.GenreDAO;
import DAO.TypeDAO;

public class ManagerController implements Initializable {
    @FXML
    AnchorPane artworkPane, artistPane, genrePane, typePane, eventPane, mainPane;
    @FXML
    JFXTextField txtGenreName, txtTypeName, txtArtistName, txtArtistFamily;
    @FXML
    JFXComboBox comboxAge;
    @FXML
    TableColumn<?, String> genre_col_name, type_col_name, artist_col_id, artist_col_name, artist_col_family, artist_col_age;
    @FXML
    TableView<Genre> genreTable;
    @FXML
    TableView<Type> typeTable;
    @FXML
    TableView<Artist> artistTable;

    private GenreDAO genreDAO;
    private TypeDAO typeDAO;
    private ArtistDAO artistDAO;

    private ObservableList<Genre> genreList = FXCollections.observableArrayList();
    private ObservableList<Type> typeList = FXCollections.observableArrayList();
    private ObservableList<Artist> artistList = FXCollections.observableArrayList();
    private DataBase db = DataBase.getConnection("root", "admin", "agm");
    private Connection connection = db.connect();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        mainPane.getChildren().stream().filter(Pane -> !(Pane instanceof VBox)).forEach(node -> {
            node.setVisible(false);
            node.setDisable(true);
        });


    }

    private void disableCurentPane() {
        for (int i = 0; i < mainPane.getChildren().size(); i++) {
            if (mainPane.getChildren().get(i) instanceof VBox) {
                continue;
            } else {
                if (mainPane.getChildren().get(i).isVisible()) {
                    mainPane.getChildren().get(i).setVisible(false);
                    mainPane.getChildren().get(i).setDisable(false);
                }
            }

        }
    }

    public void manageArtist(ActionEvent actionEvent) {
        disableCurentPane();
        artistPane.setVisible(true);
        artistPane.setDisable(false);
        if (artistDAO == null) {
            initArtistTable();
            artistDAO = new ArtistDAO(connection);
            artistList.addAll(artistDAO.findAll());
            artistTable.setItems(artistList);
        } else {
            artistTable.setItems(artistList);

        }
        for (int i = 20; i < 90; i++) {
            ObservableList<Integer> ages = FXCollections.observableArrayList();
            ages.add(i);
            comboxAge.getItems().addAll(ages);
        }
    }


    public void manageEvents(ActionEvent actionEvent) {
        disableCurentPane();
        eventPane.setVisible(true);
        eventPane.setDisable(false);

    }

    public void manageArtwork(ActionEvent actionEvent) {
        disableCurentPane();
        artworkPane.setVisible(true);
        artworkPane.setDisable(false);

    }

    public void manageGenre(ActionEvent actionEvent) {


        disableCurentPane();
        genrePane.setVisible(true);
        genrePane.setDisable(false);
        if (genreDAO == null) {
            initGenreTable();
            genreDAO = new GenreDAO(connection);
            genreList.addAll(genreDAO.findAllGenres());
            genreTable.setItems(genreList);
        } else {
            genreTable.setItems(genreList);

        }


    }

    public void manageType(ActionEvent actionEvent) {
        disableCurentPane();
        typePane.setVisible(true);
        typePane.setDisable(false);
        if (typeDAO == null) {
            initTypeTable();
            typeDAO = new TypeDAO(connection);
            typeList.addAll(typeDAO.findAllTypes());
            typeTable.setItems(typeList);
        } else {
            typeTable.setItems(typeList);

        }
    }

    private void initArtistTable() {

        artist_col_id.setCellValueFactory(new PropertyValueFactory<>("ID"));
        artist_col_name.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        artist_col_family.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        artist_col_age.setCellValueFactory(new PropertyValueFactory<>("age"));

    }

    private void initTypeTable() {
        type_col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
    }


    private void initGenreTable() {
        genre_col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
    }

    public void addType(ActionEvent actionEvent) {
        if (txtTypeName.getText() == null || txtTypeName.getText().length() < 3) {
            //Dialog Box
        } else {
            Type type = new Type(null, txtTypeName.getText());
            typeDAO.addType(type);
            typeList.add(type);


        }
    }

    public void addGenre(ActionEvent actionEvent) {
        if (txtGenreName.getText() == null || txtGenreName.getText().length() < 3) {
            //Dialog Box
        } else {
            Genre genre = new Genre(null, txtGenreName.getText());
            genreDAO.addGenre(genre);
            genreList.add(genre);

        }


    }

    public void addArtist(ActionEvent actionEvent) {
        if (txtArtistName.getText() == null || txtArtistFamily.getText() == null) {

        } else {
            Artist artist = new Artist(null, txtArtistName.getText(), txtArtistFamily.getText(),null, (Integer) comboxAge.getValue());
            artistDAO.add(artist);
            artistList.add(artist);

        }
    }

    public void deleteGenre(ActionEvent actionEvent) {
        genreDAO.removeGenre(genreTable.getSelectionModel().getSelectedItem().getName());
        genreList.remove(genreTable.getSelectionModel().getSelectedItem());
    }

    public void deleteType(ActionEvent actionEvent) {
        typeDAO.removeType(typeTable.getSelectionModel().getSelectedItem().getName());
        typeList.remove(typeTable.getSelectionModel().getSelectedItem());
    }

    public void editGenre(ActionEvent actionEvent) {

        Genre genre = new Genre(genreTable.getSelectionModel().getSelectedItem().getId(), txtGenreName.getText());
        genreDAO.editGenre(genre);
        genreList.removeAll();
        genreTable.getItems().clear();
        genreList.addAll(genreDAO.findAllGenres());

    }

    public void editType(ActionEvent actionEvent) {

        Type type = new Type(typeTable.getSelectionModel().getSelectedItem().getId(), txtTypeName.getText());
        typeDAO.editType(type);
        typeList.removeAll();
        typeTable.getItems().clear();
        typeList.addAll(typeDAO.findAllTypes());
    }

    public void GenreTableListener(MouseEvent mouseEvent) {
        if (genreTable.getSelectionModel().getSelectedItem() == null) {


        } else {
            txtGenreName.setText(genreTable.getSelectionModel().getSelectedItem().getName());
        }
    }

    public void typeTableListener(MouseEvent mouseEvent) {
        if (typeTable.getSelectionModel().getSelectedItem() == null) {


        } else {
            txtTypeName.setText(typeTable.getSelectionModel().getSelectedItem().getName());
        }
    }


}
