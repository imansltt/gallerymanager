package ntt;


public class Artist {
    public Integer id;
    private String fname;
    private String lname;
    private String email;
    private String image_src;
    private Integer age;

    public Artist(Integer id, String fname, String lname, String image_src, Integer age) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
//        this.email = email;
        this.image_src = image_src;
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

//    public String getEmail() {
//        return email;
//    }

    public String getImage_src() {
        return image_src;
    }

    public Integer getAge() {
        return age;
    }
}

