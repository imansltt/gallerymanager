package ntt;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class Event {
    public enum State {
        unknown,
        future,
        INPROGRESS,
        PAST
    }

    private Integer id;

    public Event(ArrayList<ArtWork> artWorks) {
        this.artWorks = artWorks;
    }

    private ArrayList<ArtWork> artWorks = new ArrayList<>();
    private LocalDate localDate = LocalDate.now();
    private State state = State.unknown;
    private String name;
    private String date_str;

    private static final DateTimeFormatter formatter
            = DateTimeFormatter.ofPattern("uuuu-MM-dd");


    // ########################################################################### Constructor
    public Event(Integer id, String name, String date_str, ArtWork artWork) {
        setId(id);
        artWorks.add(artWork);
        setName(name);
        setDate_str(date_str);
        setState(date_str);
    }

    public Event(Integer id, String name, String date_str, ArtWork artWork, State state) {
        setId(id);
        artWorks.add(artWork);
        setName(name);
        setDate_str(date_str);
        setState(state);
    }

    public Event(Integer id, ArrayList<ArtWork> artWorks, String date_str) {
        setId(id);
        setArtWorks(artWorks);
        setDate_str(date_str);
        setState(date_str);
    }

    public Event(Integer id, String name, String date_str) {
        setId(id);
        setName(name);
        setDate_str(date_str);
        setState(date_str);
    }

    public Event(Integer id, String name, String date_str, ArrayList<ArtWork> artWorks) {
        setId(id);
        setDate_str(date_str);
        setState(date_str);
        setArtWorks(artWorks);
        setName(name);
    }

    public Event(Integer id, String name, ArrayList<ArtWork> artWorks) {
        setId(id);
        setArtWorks(artWorks);
        setName(name);
    }

    public Event(String name) {

        setName(name);
    }

    // ########################################################################### Setter & Getter
    public void setArtWorks(ArrayList<ArtWork> artWorks) {
        this.artWorks = artWorks;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDate_str(String date_str) {
        this.date_str = date_str;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    public ArtWork getArtWork() {
        if (artWorks.size() == 1) {
            return artWorks.get(0);
        } else {
            this.getArtWorks();
        }
        return artWorks.get(0);
    }

    public ArrayList<ArtWork> getArtWorks() {
        return artWorks;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public State getState() {
        return state;
    }

    public void setState(String date_str) {
        localDate = LocalDate.parse(date_str, formatter);
        if (this.localDate.isAfter(LocalDate.now())) {
            state = State.future;
        } else if (this.localDate.isEqual(LocalDate.now())) {
            state = State.INPROGRESS;
        } else {
            state = State.PAST;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // ########################################################################### toString

    @Override
    public String toString() {
        return "Event{" +
                "artWorks=" + artWorks.toString() +
                ", localDate=" + localDate.toString() +
                ", state=" + state +
                ", name='" + name + '\'' +
                '}';
    }

//    // ########################################################################### addArtwork
//    public void addArtWork(ArtWork artWork) {
//        artWorks.add(artWork);
//    }
//
//    // ########################################################################### DeleteArtwork
//
//    public void deleteArtWork(ArtWork artWork) {
//        artWorks.remove(artWork);
//    }
//
//    // ###########################################################################
//
//
//    public void edit() {
//        //GUI
//        //choose event
//    }
}
